﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Paginador.Models;

namespace Paginador.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetInformation()
        {
            using (worldEntities dc = new worldEntities())
            {
                var city = dc.city.OrderBy(a => a.Name).ToList();
                return Json(new { data = city }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}